/* Insert into People */
INSERT INTO People (id, name) VALUES
(1, 'people1');
INSERT INTO People (id, name) VALUES
(2, 'people2');
INSERT INTO People (id, name) VALUES
(3, 'people3');
INSERT INTO People (id, name) VALUES
(4, 'people4');

/* Insert into classroom */
INSERT INTO Classroom (id, capacity, name) VALUES
(1, 52, 'class1');
INSERT INTO Classroom (id, capacity, name) VALUES
(2, 60, 'class2');
INSERT INTO Classroom (id, capacity, name) VALUES
(3, 20, 'class3');
INSERT INTO Classroom (id, capacity, name) VALUES
(4, 30, 'class4');

/* Insert into course */
INSERT INTO Course (id, date, name, classroom_id) VALUES
(1, '10/03/2022', 'math', 1);
INSERT INTO Course (id, date, name, classroom_id) VALUES
(2, '10/03/2022', 'anglais', 2);
INSERT INTO Course (id, date, name, classroom_id) VALUES
(3, '10/03/2022', 'histoire', 3);
INSERT INTO Course (id, date, name, classroom_id) VALUES
(4, '10/03/2022', 'français', 4);