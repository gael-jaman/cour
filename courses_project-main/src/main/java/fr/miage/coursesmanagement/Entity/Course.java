package fr.miage.coursesmanagement.Entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class Course {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    private String date;
    private String name;

    @ManyToOne
    private Classroom classroom;

    @ManyToMany
    private List<People> listPeople;

}
