package fr.miage.coursesmanagement.Entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class Classroom {

    @Id
    @Column(name = "id", nullable = false)
    private Long id;
    private int capacity;
    private String name;
}
