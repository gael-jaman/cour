package fr.miage.coursesmanagement.Controller;

import fr.miage.coursesmanagement.Entity.People;
import fr.miage.coursesmanagement.Repository.PeopleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/peoples")
public class PeopleController {

    private final PeopleRepository ur;

    @GetMapping
    public ResponseEntity<?> getAllPeoples(){
        Iterable<People> allUsers = ur.findAll();
        return ResponseEntity.ok(allUsers);
    }
}
