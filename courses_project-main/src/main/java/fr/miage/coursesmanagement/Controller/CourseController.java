package fr.miage.coursesmanagement.Controller;

import fr.miage.coursesmanagement.Entity.Course;
import fr.miage.coursesmanagement.Entity.People;
import fr.miage.coursesmanagement.Repository.CourseRepository;
import fr.miage.coursesmanagement.Repository.PeopleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/courses")
public class CourseController {

    private final PeopleRepository ur;
    private final CourseRepository cr;

    @GetMapping
    public ResponseEntity<?> getAllCourses(){
        Iterable<Course> allCourses = cr.findAll();
        return ResponseEntity.ok(allCourses);
    }

    @PostMapping("/{courseId}/{peopleId}")
    public ResponseEntity<?> affectPeople(@PathVariable("courseId") Long courseId, @PathVariable("peopleId") Long peopleId){
        Course course = cr.getById(courseId);

        People people = ur.getById(peopleId);

        course.getListPeople().add(people);

        cr.save(course);

        return ResponseEntity.ok().build();
    }

}
